package com.weather.testcases;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.weather.baseUtil.TestBase;
import com.weather.baseUtil.TestUtil;
import com.weather.methods.WriteCsvFile;
import com.weather.pages.WeatherForecastingpage;
import com.weather.pages.WeatherHomepage;


//import com.ztaf.utility.Utilities;

public class WeatherDotComTest extends TestBase {

	Logger log = Logger.getLogger(WeatherDotComTest.class);

	WeatherHomepage weatherHomepage;
	WeatherForecastingpage forecastpage;
	ExtentReports extent;
	ExtentTest logger;
	
	public WeatherDotComTest() {
		super();
	}

	
	@Parameters({"browserName"})
	@BeforeMethod
	public void setup(String browserName) {
		
		initialisation(browserName);
		System.out.println("Thread id: "+Thread.currentThread().getId());
		weatherHomepage =new WeatherHomepage();
		
		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Reports/WeatherExtentReport.htlm");
		extent = new ExtentReports();
		
		extent.attachReporter(reporter);
		logger = extent.createTest("SearchCityTest");
	}

	@DataProvider
	public Object[][] getcities(){
		Object testdata[][] = TestUtil.getTestData();
		return testdata;
	}
	
	@Test(dataProvider = "getcities")
	public void weatherSearch(String city, String state) throws Exception {
			
		logger.log(Status.INFO, "Searching location in weather.com");
   
		forecastpage = weatherHomepage.searchCity(city, state);
		
		WriteCsvFile writer = new WriteCsvFile(prop,city, state); 
		writer.addRow(forecastpage);

		logger.log(Status.PASS, "Temperatures copied to CSV");
   
        // Flush method will write the test in report- This is mandatory step  
		extent.flush();
		
	}
	
	/*
	 * @Test public void accuWeatherSearch() throws Exception {
	 * 
	 * logger.log(Status.INFO, "Searching location in weather.com");
	 * 
	 * forecastpage = weatherHomepage.searchCity();
	 * 
	 * logger.log(Status.PASS, "Temperatures copied to CSV");
	 * 
	 * // Flush method will write the test in report- This is mandatory step
	 * extent.flush();
	 * 
	 * }
	 */
	
	@AfterMethod
	public void tearDown() {
		termination();
	}
}	
/*
 * @Test(dependsOnMethods = "searchCity") public void titleTest() {
 * 
 * String title = driver.getTitle(); System.out.println(title);
 * Assert.assertEquals(title,
 * "Amravati, MaharashtraWeather Forecast and Conditions - The Weather Channel | Weather.com"
 * , "Title Not Matched");
 * 
 * }
 */

