package com.weather.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.weather.baseUtil.TestBase;
import com.weather.methods.ReadCityFromCsv;
import com.weather.methods.ReadCityFromExcel;
import com.weather.methods.WriteCsvFile;


public class WeatherHomepage extends TestBase {

	Logger log = Logger.getLogger(getClass());
	
	  //page Factory
	  
	  @FindBy(id="LocationSearch_input") 
	  WebElement searchBar;
	  
	  @FindBy(xpath="//*[@id='LocationSearch_listbox']//button[@role='option']") 
	  List<WebElement> locationSearchlistBox;
	 
		
		/*
		 * private static WebElement searchBarElement = null; private static
		 * List<WebElement> listBoxElement = null;
		 * 
		 * public static WebElement searchBar() { searchBarElement =
		 * driver.findElement(By.id("LocationSearch_input")); return searchBarElement; }
		 * 
		 * public static List<WebElement> listBox() { listBoxElement =
		 * driver.findElements(By.xpath(
		 * "//*[@id='LocationSearch_listbox']//button[@role='option']")); return
		 * listBoxElement; }
		 */
	
	
	  public WeatherHomepage() {
	  
	  PageFactory.initElements(driver, this);
	  
	  }
	 
	 public void enterCityName(String city) {
		
		//Explicitly wait for the element to be loaded
		TestBase.explicitlyWait(searchBar, Integer.parseInt(prop.getProperty("explicitly_Wait")));
		searchBar.clear();
		//driver.findElement(By.id("LocationSearch_input")).sendKeys(city);
		searchBar.sendKeys(city);
		
	 }
	
	 public void searchInLoacationBox(String city, String state) {
		
		System.out.println(locationSearchlistBox);
		log.info("Total number of suggetions = " + locationSearchlistBox.size());

		// Iterate to search city in suggetion box
		for (int i = 0; i < locationSearchlistBox.size(); i++) 
		{
			log.info(locationSearchlistBox.get(i).getText());
			if (locationSearchlistBox.get(i).getText().contains(city + ", " + state)) {
				locationSearchlistBox.get(i).click();
				break;
			}
		}
	 }
	
	 public WeatherForecastingpage searchCity(String city, String state) throws Exception, InterruptedException {
			
			if(prop.getProperty("filetype").equals("csv"))
			{
				log.info("Reading data from CSV file");
				enterCityName(city);
				Thread.sleep(Integer.parseInt(prop.getProperty("sleep1")));
				searchInLoacationBox(city,state);
				//Thread.sleep(Integer.parseInt(prop.getProperty("sleep1")));
			}
			else {
				log.info("Reading data from Excel file");
				ReadCityFromExcel reader1 = new ReadCityFromExcel(prop, driver);
				//log.info("ReadCityFromXlsx class object initialized");
				reader1.searchcity();
				
			}
			return new WeatherForecastingpage();
		}
	
}
