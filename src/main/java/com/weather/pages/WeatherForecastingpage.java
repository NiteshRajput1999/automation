package com.weather.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.weather.baseUtil.TestBase;

public class WeatherForecastingpage extends TestBase {

	Logger log = Logger.getLogger(getClass());
	Date todaysDate = new Date();
    DateFormat df = new SimpleDateFormat("E dd");
	
	
	//Page factory
	@FindBy(xpath="//span[@data-testid='TemperatureValue' and @class='CurrentConditions--tempValue--3KcTQ']") 
	  WebElement todaysTemp;
	  
	@FindBy(xpath="//section[@data-testid='DailyWeatherModule']//span[@data-testid='TemperatureValue']") 
	  List<WebElement> tempslist;
	
	@FindBy(xpath="//section[@data-testid='DailyWeatherModule']//span[@class='Ellipsis--ellipsis--lfjoB']") 
	  List<WebElement> datesList;
	
	public WeatherForecastingpage() {
		  
		  PageFactory.initElements(driver, this);
		  
		  }
		/*
		 * private static WebElement todayTemp=null; private static List <WebElement>
		 * tempList=null; private static List <WebElement> datesList=null;
		 * 
		 * public static WebElement todaysTemp() {
		 * todayTemp=driver.findElement(By.xpath()); return todayTemp; } public static
		 * List<WebElement> tempslist() { tempList=driver.findElements(By.xpath());
		 * return tempList; } public static List<WebElement> dateslist() {
		 * datesList=driver.findElements(By.xpath()); return datesList; }
		 */
	
	public String[] setTempratures(String city,String state) {
		
		 //set data into variables
		String todaydate = df.format(todaysDate);
        String todayTemp = todaysTemp.getText();
        String[] tempratures=new String[10];
        String[] dates=new String[5];
        for(int i=1;i<tempslist.size();i++) 
        {
        	tempratures[i]=tempslist.get(i).getText();
        }
        for(int i=1;i<datesList.size();i++) 
        {
        	dates[i]=datesList.get(i).getText();
        }
       String[] RowData = {city,state,todaydate,todayTemp,tempratures[1],dates[1],tempratures[2],tempratures[3],dates[2],tempratures[4],tempratures[5]};
       return RowData;
	}
	
}
