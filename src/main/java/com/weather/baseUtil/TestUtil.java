package com.weather.baseUtil;

import java.io.FileReader;

import com.opencsv.CSVReader;
import com.weather.methods.ReadCityFromCsv;
import com.weather.methods.ReadCityFromExcel;
import com.weather.pages.WeatherForecastingpage;

public class TestUtil extends TestBase {

	public static Object[][] getTestData() {
		Object[][] data = null;
		
		if(prop.getProperty("filetype").equals("csv"))
		{
			log.info("Reading data from CSV file");
			
			ReadCityFromCsv reader = new ReadCityFromCsv(prop);
			data = reader.searchcity();
			
		}
		else {
			/*
			 * log.info("Reading data from Excel file"); ReadCityFromExcel reader1 = new
			 * ReadCityFromExcel(prop, driver);
			 * //log.info("ReadCityFromXlsx class object initialized");
			 * reader1.searchcity(); return new WeatherForecastingpage();
			 */
		}
		return data;
		
	}

}
