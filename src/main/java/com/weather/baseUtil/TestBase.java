package com.weather.baseUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;


public class TestBase{

	static Logger log = Logger.getLogger(TestBase.class);

	public static WebDriver driver;
	public static Properties prop;
	static String path = System.getProperty("user.dir");
	
	
	public TestBase(){
		
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(path+"\\src\\main\\java\\com\\weather\\config\\config.properties");
			prop.load(ip);
			log.info("Config.properties set");
		} catch (FileNotFoundException e) {
			log.error("Kindly check whether File is found in the specified location"+e.getMessage()+e.getStackTrace());
			e.printStackTrace();

		} catch (IOException e) {
			log.error("File is not found in the specified location"+e.getMessage()+e.getStackTrace());
			e.printStackTrace();
		}
	}
	
	public static void initialisation(String browser) {
		
		String browsername=browser;
		
		if (browsername.equals("chrome")) {
			WebDriverManager.chromedriver().setup();			
			driver = new ChromeDriver();
			log.info("Chrome driver initialized");
		
		} else if (browsername.equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			log.info("Firefox driver initialized");
		}
		else if (browsername.equals("safari")) {
			driver = new SafariDriver();
			log.info("Safari driver initialized");
		}
		else if (browsername.equals("ie")) {
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
			log.info("Internet Explorer driver initialized");
		}
		else if (browsername.equals("edge")) {
			WebDriverManager.edgedriver().setup();
			driver = new InternetExplorerDriver();
			log.info("Microsoft Edge driver initialized");
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(Integer.parseInt(prop.getProperty("pageLoad_Timeout")), TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(prop.getProperty("implicitly_Wait")), TimeUnit.SECONDS);
		log.info("Window set");
		driver.get(prop.getProperty("url"));
		log.info("URL Entered");
	}

	public static  void explicitlyWait(WebElement element, int timeout) {
		new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
		
	}
	
	public static void termination() {
		driver.close();
		log.info("Browser closed..");
	}
}