package com.weather.methods;

import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.weather.baseUtil.TestBase;


public class ReadCityFromExcel {
	
	Logger log = Logger.getLogger(ReadCityFromExcel.class);
	String city;
	String state;
	WebDriver driver;
	Properties prop;
	static String path = System.getProperty("user.dir");
	
	public ReadCityFromExcel(Properties prop,WebDriver driver) {
		this.driver = driver;
		this.prop = prop;
	} 
	
	
	public void searchcity() {
		
		try{
			
			Xls_Reader_Utilities reader = new Xls_Reader_Utilities(path+prop.getProperty("readExcelXPath"));
			int rowcount = reader.getRowCount("sheet1");
			
			for(int rowNum=2;rowNum<=rowcount;rowNum++) {
				
			}
			city = reader.getCellData("sheet1", "city", 2);
			log.info("city= " + city);
			state = reader.getCellData("sheet1", "state", 2);
			log.info("state= " + state);
			
			WebElement serchbar = driver.findElement(By.id("LocationSearch_input"));
			//Explicitly wait for the element to be loaded
			TestBase.explicitlyWait(serchbar, 15);
			
			serchbar.clear();
			serchbar.sendKeys(city);
			Thread.sleep(2000);
			List<WebElement> list = driver.findElements(By.xpath("//*[@id='LocationSearch_listbox']//button[@role='option']"));

			log.info("Total number of suggetions = " + list.size());

			// Iterate to search city in suggetion box
			for (int i = 0; i < list.size(); i++) 
			{
				log.info(list.get(i).getText());
				if (list.get(i).getText().contains(city + ", " + state)) {
					list.get(i).click();
					break;
				}
			}
			Thread.sleep(Integer.parseInt(prop.getProperty("sleep1")));
			WriteExcelFile writer = new WriteExcelFile(prop,driver,city, state);
			writer.addRow();
		
		}catch (Exception e) { 
	        e.printStackTrace(); 
	    } 
		
		
		
	}
	
}
