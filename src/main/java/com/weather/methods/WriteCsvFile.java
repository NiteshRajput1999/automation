package com.weather.methods;

import java.io.FileWriter;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.opencsv.CSVWriter;
import com.weather.pages.WeatherForecastingpage;

public class WriteCsvFile
{
	Logger log = Logger.getLogger(WriteCsvFile.class);
	String city;
	String state;
	Properties prop;
	static String path = System.getProperty("user.dir");
	
	public WriteCsvFile(Properties prop,String city, String state) {
		this.city = city;
		this.state = state;
		this.prop=prop;
	}
	
	
	public void addRow(WeatherForecastingpage forecast) throws Exception {
		
		
		log.info(city+" Weather Forecast and condition page opened");
		
		try
	    {
	        FileWriter fw = new FileWriter(path+prop.getProperty("writeCSVPath"),true);
	        // create CSVWriter object filewriter object as parameter 
	        CSVWriter writer = new CSVWriter(fw); 
	        String[] rowdata = forecast.setTempratures(city, state);
	        writer.writeNext(rowdata);
	        log.info("CSV file is Updated successfully for "+city+" ,"+state);
	        
	        writer.close();
	        fw.flush();
	        fw.close();
	       
	    }
	    catch(Exception e)
	    {
	        System.out.println(e.getMessage());
	    }    
		
	}
}
