package com.weather.methods;

import java.io.FileReader;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.opencsv.CSVReader;
import com.weather.pages.WeatherHomepage;
//import com.ztaf.utility.Utilities;

public class ReadCityFromCsv {
	
	Logger log = Logger.getLogger(ReadCityFromCsv.class);
	Properties prop;
	Object[][] data = null;
	static String path = System.getProperty("user.dir");
	
	public ReadCityFromCsv(Properties prop) 
	{
		this.prop = prop;
	}
	
	public Object[][] searchcity() {
		
		String[] nextRecord;
		int i = 0;
		
		try{ 
			data = new Object[6][2];
			FileReader filereader = new FileReader(path+prop.getProperty("readCSVPath"));
			CSVReader csvReader = new CSVReader(filereader);
			
			log.info("Data readed from ReadCityName.csv file");
			csvReader.readNext();
			
			// we are going to read data line by line
			//(nextRecord = csvReader.readNext()) != null
			while ((nextRecord = csvReader.readNext()) != null) {
				
				data[i][0]= nextRecord[0]; 
				data[i][1]=	nextRecord[1];
				i++;
				/*
				 * String city = nextRecord[0]; String state = nextRecord[1];
				 * 
				 * WeatherHomepage weatherHomepage = new WeatherHomepage();
				 * weatherHomepage.enterCityName(city);
				 * 
				 * Thread.sleep(Integer.parseInt(prop.getProperty("sleep1")));
				 * weatherHomepage.searchInLoacationBox(city,state);
				 * 
				 * Thread.sleep(Integer.parseInt(prop.getProperty("sleep1"))); WriteCsvFile
				 * writer = new WriteCsvFile(prop,city, state); writer.addRow();
				 */
	        }
			csvReader.close();
			return data;
	    } 
			
	    catch (Exception e) { 
	        e.printStackTrace(); 
	    }
		return data; 
		
	}
}

